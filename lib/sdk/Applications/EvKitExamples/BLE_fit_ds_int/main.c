/*******************************************************************************
 * Copyright (C) 2018 Maxim Integrated Products, Inc., All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL MAXIM INTEGRATED BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of Maxim Integrated
 * Products, Inc. shall not be used except as stated in the Maxim Integrated
 * Products, Inc. Branding Policy.
 *
 * The mere transfer of this software does not imply any licenses
 * of trade secrets, proprietary technology, copyrights, patents,
 * trademarks, maskwork rights, or any other form of intellectual
 * property whatsoever. Maxim Integrated Products, Inc. retains all
 * ownership rights.
 *
 * $Date: 2020-09-24 11:51:19 -0500 (Thu, 24 Sep 2020) $
 * $Revision: 55916 $
 *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "mxc_config.h"
#include "nvic_table.h"
#include "wsf_types.h"
#include "wsf_os.h"
#include "wsf_buf.h"
#include "wsf_timer.h"
#include "wsf_trace.h"
#include "app_ui.h"
#include "fit/fit_api.h"
#include "app_ui.h"
#include "hci_vs.h"
#include "hci_core.h"
#include "hci_drv_sdma.h"
#include "ipc_defs.h"
#include "pb.h"
#include "tmr.h"
#include "wakeup.h"
#include "uart.h"
#include "sch_api.h"
#include "wut.h"
#include "simo.h"
#include "board.h"
#include "gcr_regs.h"
#include "sch_api_ble.h"
#include "lp.h"
#include "bb_drv.h"
#include "led.h"
#include "tmr_utils.h"
#include "pal_rtc.h"
#include "dm_api.h"
#include "att_api.h"
#include "bas_user.h"

/**************************************************************************************************
  Macros
**************************************************************************************************/

// OST_TIMER defined in bas_user.h
   
/* Size of buffer for stdio functions */
#define WSF_BUF_POOLS       6
#define WSF_BUF_SIZE        0x1048

/* Size of buffer for stdio functions */
#define PRINTF_BUF_SIZE     128

/* Definitions for push button handling */
#define BUTTON0_TMR         MXC_TMR0
#define BUTTON1_TMR         MXC_TMR1
#define BUTTON_SHORT_MS     200
#define BUTTON_MED_MS       500
#define BUTTON_LONG_MS      1000

/* **************************************** */
/* Application definitions */

/* Definitions for demo LED assignment */

/// @brief Specifies the LED used to indicate when the part is in deepsleep mode.
#define DEEPSLEEP_LED       0

/// @brief Specifies the LED used to indicate when a user event is pending.
#define USER_GPIO_LED       1

/* Timing definitions */

/// @brief Specifies the time slice that the scheduler needs
/// to be idle for user event handling to occur.
//#define EVENT_TIME_REQUIREMENT (100) // 100us required to handle event.
#undef EVENT_TIME_REQUIREMENT // Don't check for time to handle event.

/// @brief Specifies the time the user event handler
/// is to wait to simulate performing actual work.
/// This must be shorter than EVENT_TIME_REQUIREMENT for
/// the BLE operation to not be disrupted.
//#define EVENT_TIME_SIM_DELAY (50) // 50us delay to simulate event work.
//#define EVENT_TIME_SIM_DELAY (-1000) // 1000 instruction (min) delay to simulate event work.
#undef EVENT_TIME_SIM_DELAY // No simulated delay

/* Pin assignments for input event */

/// @brief Specifies the GPIO port used to signal a user event.
#define USER_PIN_PORT PORT_1

/// @brief Specifies the GPIO pin used to signal a user event.
#define USER_PIN_PIN PIN_6

/// @brief Specifies the GPIO pin pullup configuration for
/// the pin used to signal user events.
#define USER_PIN_PAD GPIO_PAD_PULL_UP

/// @brief Enable handling of user events indicated using GPIO interrupt in main loop.
#define MAIN_LOOP_HANDLER

/// @brief Enable handling of user events indicated using GPIO interrupt in sleep loop.
//#define SLEEP_LOOP_HANDLER

/// @brief Force wsf dispatch.
#define USE_FORCE_WSF_DISPATCH

/* Demo selection */

/// @brief Specifies that the LED be toggled each time a user event is handled.
#undef USER_GPIO_TOGGLE_DEMO

/// @brief Specifies that the LED be turned on when an led interrupt occurs,
/// and off when the event is handled.
#define USER_GPIO_TIMING_DEMO

/**************************************************************************************************
  Local Variables
**************************************************************************************************/

uint32_t SystemHeapSize=WSF_BUF_SIZE;
uint32_t SystemHeap[WSF_BUF_SIZE/4];
uint32_t SystemHeapStart;

/*! Buffer for stdio functions */
char printf_buffer[PRINTF_BUF_SIZE];

/*! Default pool descriptor. */
static wsfBufPoolDesc_t mainPoolDesc[WSF_BUF_POOLS] = {
    {  16,  8 },
    {  32,  4 },
    {  64,  4 },
    { 128,  4 },
    { 256,  4 },
    { 512,  4 }
};

/**
 * @brief User pin configuration. */
const gpio_cfg_t user_pin[USER_PIN_COUNT] = {
    /* USER_PIN_IN: */
    {
        USER_PIN_PORT,
        USER_PIN_PIN,
        GPIO_FUNC_IN,
        USER_PIN_PAD
    },
    /* USER_PIN_OUT: */
    { 0 } // Default to using led_pin[USER_GPIO_LED].
};

/**************************************************************************************************
  Functions
**************************************************************************************************/

/*! \brief  Stack initialization for app. */
extern void StackInitFit(void);

/*************************************************************************************************/
void SysTick_Handler(void)
{
    WsfTimerUpdate(WSF_MS_PER_TICK);
}

/*************************************************************************************************/
static bool_t myTrace(const uint8_t *pBuf, uint32_t len)
{
    extern uint8_t wsfCsNesting;

    if (wsfCsNesting == 0) {
        fwrite(pBuf, len, 1, stdout);
        return TRUE;
    }

    return FALSE;
}

/*************************************************************************************************/
/*!
 *  \brief  Initialize WSF.
 *
 *  \return None.
 */
/*************************************************************************************************/
static void WsfInit(void)
{
    uint32_t bytesUsed;
    /* setup the systick for 1MS timer*/
    SysTick->LOAD = (SystemCoreClock / 1000) * WSF_MS_PER_TICK;
    SysTick->VAL = 0;
    SysTick->CTRL |= (SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_TICKINT_Msk);

    WsfTimerInit();

    SystemHeapStart = (uint32_t)&SystemHeap;
    memset(SystemHeap, 0, sizeof(SystemHeap));
    printf("SystemHeapStart = 0x%x\n", SystemHeapStart);
    printf("SystemHeapSize = 0x%x\n", SystemHeapSize);
    bytesUsed = WsfBufInit(WSF_BUF_POOLS, mainPoolDesc);
    printf("bytesUsed = 0x%x\n", bytesUsed);

    WsfTraceRegisterHandler(myTrace);
    WsfTraceEnable(TRUE);
}

/*
 * In two-chip solutions, setting the address must wait until the HCI interface is initialized.
 * This handler can also catch other Application events, but none are currently implemented.
 * See ble-profiles/sources/apps/app/common/app_ui.c for further details.
 *
 */
void SetAddress(uint8_t event)
{
    uint8_t bdAddr[6] = {0x02, 0x02, 0x44, 0x8B, 0x05, 0x00};

    switch (event) {
        case APP_UI_RESET_CMPL:
            printf("Setting address -- MAC %02X:%02X:%02X:%02X:%02X:%02X\n", bdAddr[5], bdAddr[4], bdAddr[3], bdAddr[2], bdAddr[1], bdAddr[0]);
            HciVsSetBdAddr(bdAddr);
            break;
        default:
            break;
    }
}

/**
 * @brief  Wakeup timer hook.
 */
void WUT_IRQHook(void * pHookArg)
{
  // This argument isn't used for this application.
  // Explicitly ignore.
  (void)pHookArg;

  bHaveWUTEvent = -1;
}

/*************************************************************************************************/
void HandleButton(int button)
{
    mxc_tmr_regs_t* button_tmr = MXC_TMR_GET_TMR(button);

    /* Check if rising or falling */
    if(PB_Get(button)) {
        /* Start timer */
        TMR_Enable(button_tmr);
    } else {
        uint32_t time;
        tmr_unit_t unit;

        /* Get the elapsed time since the button was pressed */
        TMR_GetTime(button_tmr, TMR_GetCount(button_tmr), &time, &unit);
        TMR_Disable(button_tmr);
        TMR_SetCount(button_tmr, 0);

        if(unit == TMR_UNIT_NANOSEC) {
            time /= 1000000;
        } else if(unit == TMR_UNIT_MICROSEC) {
            time /= 1000;
        }

        if(time < BUTTON_SHORT_MS) {
            AppUiBtnTest(button ? APP_UI_BTN_2_SHORT : APP_UI_BTN_1_SHORT);
        } else if (time < BUTTON_MED_MS) {
            AppUiBtnTest(button ? APP_UI_BTN_2_MED : APP_UI_BTN_1_MED);
        } else if (time < BUTTON_LONG_MS) {
            AppUiBtnTest(button ? APP_UI_BTN_2_LONG : APP_UI_BTN_1_LONG);
        } else {
            AppUiBtnTest(button ? APP_UI_BTN_2_EX_LONG : APP_UI_BTN_1_EX_LONG);
        }
    }
}

/*************************************************************************************************/
void Button0Pressed(void* arg)
{
    HandleButton(0);
}

/*************************************************************************************************/
void Button1Pressed(void* arg)
{
    HandleButton(1);
}

/*************************************************************************************************/
void UserEvent(void)
{
    const gpio_cfg_t * pUserPinOutCfg;
    gpio_cfg_t zeroCfg = {0};
    mxc_gpio_regs_t * pUserPinOutReg;

    // Get the gpio configuration for the output pin.
    pUserPinOutCfg = &user_pin[USER_PIN_OUT];
    if (memcmp(pUserPinOutCfg, &zeroCfg, sizeof(zeroCfg)) == 0) {
        pUserPinOutCfg = &led_pin[USER_GPIO_LED];
    }
    pUserPinOutReg = MXC_GPIO_GET_GPIO(pUserPinOutCfg->port);

#if defined(MAIN_LOOP_HANDLER) || defined(SLEEP_LOOP_HANDLER)
    bHaveUserEvent = -1;
#endif /* defined(MAIN_LOOP_HANDLER) || defined(SLEEP_LOOP_HANDLER) */
#ifdef USER_GPIO_LED
#ifndef USER_GPIO_TOGGLE_DEMO
#ifdef USER_GPIO_TIMING_DEMO
    //LED_On(USER_GPIO_LED);
    pUserPinOutReg->out_clr = pUserPinOutCfg->mask;
#endif /* USER_GPIO_TIMING_DEMO */
#endif /* USER_GPIO_TOGGLE_DEMO */
#endif /* USER_GPIO_LED */
}

void UserEventPoll(void)
{
    int bCurUserLevel;

    const gpio_cfg_t * pUserPinInCfg;
    mxc_gpio_regs_t * pUserPinInReg;

    // Get the gpio configuration for the input pin.
    pUserPinInCfg = &user_pin[USER_PIN_IN];
    pUserPinInReg = MXC_GPIO_GET_GPIO(pUserPinInCfg->port);

    // Get the current user pin level.
    bCurUserLevel = (((pUserPinInReg->in & pUserPinInCfg->mask) == 0) ? 0 : 1);

    // If the level hasn't changed, then this is a non-event.
    if (bLastUserLevel == bCurUserLevel) return;

    // Process event.
    if (bCurUserLevel == 0)
    {
        UserEvent();
    }

    // Update prior observed pin level.
    bLastUserLevel = bCurUserLevel;
}

void UserEventInterrupt(void* arg)
{
    int bCurUserLevel;

    const gpio_cfg_t * pUserPinInCfg;
    mxc_gpio_regs_t * pUserPinInReg;

    // If the last interrupt is still pending, don't handle this interrupt.
    if (bHaveUserEvent != 0) return;

    // Get the gpio configuration for the input pin.
    pUserPinInCfg = &user_pin[USER_PIN_IN];
    pUserPinInReg = MXC_GPIO_GET_GPIO(pUserPinInCfg->port);

    // Get the current user pin level.
    bCurUserLevel = (((pUserPinInReg->in & pUserPinInCfg->mask) == 0) ? 0 : 1);

    // Process event.
    if (bCurUserLevel == 0)
    {
        UserEvent();
    }

    // Update prior observed pin level.
    bLastUserLevel = bCurUserLevel;
}

/*************************************************************************************************/
void RegisterUserEvent()
{
    // Register callback
    GPIO_RegisterCallback(&user_pin[USER_PIN_IN], UserEventInterrupt, (void*)0);

    // Configure and enable interrupt
    GPIO_IntConfig(&user_pin[USER_PIN_IN], GPIO_INT_EDGE, GPIO_INT_BOTH);
    GPIO_IntEnable(&user_pin[USER_PIN_IN]);
    GPIO_WakeEnable(&user_pin[USER_PIN_IN]);
    NVIC_EnableIRQ((IRQn_Type)MXC_GPIO_GET_IRQ(user_pin[USER_PIN_IN].port));
}

/*************************************************************************************************/
#if defined(MAIN_LOOP_HANDLER) || defined(SLEEP_LOOP_HANDLER)
void __attribute__((optimize("O0"))) HandleUserEvent(void)
{
    const gpio_cfg_t * pUserPinOutCfg;
    gpio_cfg_t zeroCfg = {0};
    mxc_gpio_regs_t * pUserPinOutReg;

    bool_t dueValid;
    int sleep_ticks_available;
    int sleep_ticks_required;

#ifdef EVENT_TIME_REQUIREMENT
    uint32_t  nextDbbEventDue;
#endif /* EVENT_TIME_REQUIREMENT */

    // Get the gpio configuration for the output pin.
    pUserPinOutCfg = &user_pin[USER_PIN_OUT];
    if (memcmp(pUserPinOutCfg, &zeroCfg, sizeof(zeroCfg)) == 0) {
        pUserPinOutCfg = &led_pin[USER_GPIO_LED];
    }
    pUserPinOutReg = MXC_GPIO_GET_GPIO(pUserPinOutCfg->port);

    if (bHaveUserEvent) {
        // How much time is available to handle event?
#ifdef EVENT_TIME_REQUIREMENT
        dueValid = SchBleGetNextDueTime(&nextDbbEventDue);
        sleep_ticks_available = nextDbbEventDue - BbDrvGetCurrentTime();
        sleep_ticks_required = US_TO_BBTICKS(EVENT_TIME_REQUIREMENT);
#else /* EVENT_TIME_REQUIREMENT */
        dueValid = 0;
        sleep_ticks_available = 0;
        sleep_ticks_required = 0;
#endif /* EVENT_TIME_REQUIREMENT */

        // If there is enough time, handle event.
        if (!dueValid || (sleep_ticks_available > sleep_ticks_required)) {

#ifdef EVENT_TIME_REQUIREMENT
#ifdef EVENT_TIME_SIM_DELAY
#if EVENT_TIME_SIM_DELAY > 0
            // Simulate delay required to handle event.
            TMR_Delay(OST_TIMER, EVENT_TIME_SIM_DELAY, 0);  
#else /* EVENT_TIME_SIM_DELAY > 0 */
            for (volatile int iter = 0; iter < -(EVENT_TIME_SIM_DELAY); iter++) { }
#endif /* EVENT_TIME_SIM_DELAY > 0 */
#endif /* EVENT_TIME_SIM_DELAY */
#endif /* EVENT_TIME_REQUIREMENT */

            // If there is enough time, handle event.
#ifdef USER_GPIO_LED
#ifdef USER_GPIO_TOGGLE_DEMO
            //LED_Toggle(USER_GPIO_LED);
            if (pUserPinOutReg->out & pUserPinOutCfg->mask)
                pUserPinOutReg->out_clr = pUserPinOutCfg->mask;
            else
                pUserPinOutReg->out_set = pUserPinOutCfg->mask;
#elif defined(USER_GPIO_TIMING_DEMO)
            //LED_Off(USER_GPIO_LED);
            pUserPinOutReg->out_set = pUserPinOutCfg->mask;
#endif /* USER_GPIO_*_DEMO */
#endif /* USER_GPIO_LED */

            // Clear pending event flag.
            bHaveUserEvent = 0;
        }
    }
}
#endif /* defined(MAIN_LOOP_HANDLER) || defined(SLEEP_LOOP_HANDLER) */

/*************************************************************************************************/
void Sleep(void)
{
    int bGoBackToSleep = -1;

    WsfTaskLock();

#ifdef DEEPSLEEP_LED
    LED_On(DEEPSLEEP_LED);
#endif /* DEEPSLEEP_LED */

    /* WSF and UART are not busy ? */
    if (wsfOsReadyToSleep() && UART_PrepForSleep(MXC_UART_GET_UART(CONSOLE_UART)) == E_NO_ERROR) {
        /* get next due time and sleep time */
        uint32_t  nextDbbEventDue;
        bool_t dueValid = SchBleGetNextDueTime(&nextDbbEventDue);
        int sleep_ticks = nextDbbEventDue - BbDrvGetCurrentTime();

        /* timing valid ? */
        if(dueValid && (sleep_ticks > 0 )) {
            /* have enough time to deep sleep ? */
            if(sleep_ticks > US_TO_BBTICKS(DS_WAKEUP_TIME_US + MIN_DEEPSLEEP_US)) {

                /* Stop SysTick */
                SysTick->CTRL = 0;

                /* save DBB, WUT clocks, arm WUT for wakeup */
                WUT_SetWakeup(sleep_ticks - US_TO_BBTICKS(DS_WAKEUP_TIME_US));

                /* unschedule next BLE operation */
                SchSleep();

                /* Only count WUT interrupts that follow. */
                bHaveWUTEvent = 0;

                /* Enter the appropriate sleep mode. */
                do {
#ifdef NORMAL_SLEEP
                    // Enter normal sleep.
                    LP_EnterSleepMode();

                    // If a wakeup interrupt has happened.
                    if (bHaveWUTEvent) {
                        bGoBackToSleep = 0;
                    }
#else /* NORMAL_SLEEP */
                    /* enterDeepSleep mode */
                    EnterDeepsleep();

                    /* initial restore */
                    ExitDeepsleep();

#ifdef SLEEP_LOOP_HANDLER
                    // Handle user event signaled by GPIO interrupt.
                    HandleUserEvent();
#endif /* SLEEP_LOOP_HANDLER */

#ifdef USE_FORCE_WSF_DISPATCH
                    // Leave the sleep function regardless of
                    // how much time is remaining. Wake SDMA
                    // back up and allow SDMA to decide whether
                    // returning to sleep makes sense.
                    bGoBackToSleep = 0;
#else /* USE_FORCE_WSF_DISPATCH */
                    // If a wakeup interrupt has happened.
                    if (bHaveWUTEvent) {
                        bGoBackToSleep = 0;
                    }
#endif /* USE_FORCE_WSF_DISPATCH */
#endif /* NORMAL_SLEEP */
                } while (bGoBackToSleep);

                /* Restore BLE hardware state */
                BbDrvInitWake();

                /* Restore BB clock */
                WUT_RestoreBBClock(BB_CLK_RATE_HZ);
                WsfTimerUpdate((WUT_GetSleepTicks() * 1000) / SYS_WUT_GetFreq() / WSF_MS_PER_TICK );

                /* setup the systick */
                SysTick->LOAD = (SystemCoreClock / 1000) * WSF_MS_PER_TICK;
                SysTick->VAL = 0;
                SysTick->CTRL |= (SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_TICKINT_Msk);

                /* Re-schedule next BLE operation */
                SchWake();

                /* have enough time to sleep ?*/
            } else {
                LP_EnterSleepMode();
            }
            /* Nothing scheduled, wait for interrupt */
        } else {
            LP_EnterSleepMode();
        }
    } /* if(not busy) */

#ifdef DEEPSLEEP_LED
    LED_Off(DEEPSLEEP_LED);
#endif /* DEEPSLEEP_LED */

    WsfTaskUnlock();
}

/*************************************************************************************************/
void SDMASleep(void)
{
    int bGoBackToSleep = -1;

    WsfTaskLock();

    /* Signal to the SDMA that the ARM core will restart from sleep */
    SDMASetARMFlag(ARM_FLAG_SLEEP);

#ifdef DEEPSLEEP_LED
    LED_On(DEEPSLEEP_LED);
#endif /* DEEPSLEEP_LED */

    if (wsfOsReadyToSleep() && UART_PrepForSleep(MXC_UART_GET_UART(CONSOLE_UART)) == E_NO_ERROR) {
        uint8_t sdmaFlag = SDMAGetSDMAFlag();

        if((sdmaFlag == SDMA_FLAG_SLEEPING) || (sdmaFlag == SDMA_FLAG_RUNNING)) {

            /* Store the wakeup timer value to restore the WSF timer once we wakeup */
            WUT_Store();

            /* Stop the Systick interrupt */
            SysTick->CTRL = 0;

            /* Set the wakeup compare value from the SDMA */
            WUT_SetCompare(SDMAGetWakeupTime());

            /* Only count WUT interrupts that follow. */
            bHaveWUTEvent = 0;

            /* Enter the appropriate sleep mode. */
            do {
#ifdef NORMAL_SLEEP
                // Enter normal sleep.
                LP_EnterSleepMode();

                // If a wakeup interrupt has happened.
                if (bHaveWUTEvent) {
                    bGoBackToSleep = 0;
                }
#else /* NORMAL_SLEEP */
                if(sdmaFlag == SDMA_FLAG_SLEEPING) {
                    /* Enter Deep Sleep if the SDMA is sleeping */
                    EnterDeepsleepSDMA();
                    ExitDeepsleepSDMA();
                } else {
                    /* SDMA will get stuck waiting for ARM to clear GPIO interrupt
                     * flag so disable here and poll GPIO on next wakeup. */
                    NVIC_DisableIRQ((IRQn_Type)MXC_GPIO_GET_IRQ(user_pin[USER_PIN_IN].port));
                    /* Enter Background mode if the SDMA is running */
                    EnterBackground();
                    ExitBackground();

                    /* Check to see if GPIO level has changed on wakeup.
                     * Reenable GPIO interrupts going forward where required. */
                    UserEventPoll();
                    NVIC_EnableIRQ((IRQn_Type)MXC_GPIO_GET_IRQ(user_pin[USER_PIN_IN].port));
                }

#ifdef SLEEP_LOOP_HANDLER
                // Handle user event signaled by GPIO interrupt.
                HandleUserEvent();
#endif /* SLEEP_LOOP_HANDLER */

#ifdef USE_FORCE_WSF_DISPATCH
                // Leave the sleep function regardless of
                // how much time is remaining. Wake SDMA
                // back up and allow SDMA to decide whether
                // returning to sleep makes sense.
                bGoBackToSleep = 0;
#else /* USE_FORCE_WSF_DISPATCH */
                // If a wakeup interrupt has happened.
                if (bHaveWUTEvent) {
                    bGoBackToSleep = 0;
                }
#endif /* USE_FORCE_WSF_DISPATCH */
#endif /* NORMAL_SLEEP */
            } while (bGoBackToSleep);

            /* If the SDMA was stopped, restart here. */
            if (sdmaFlag == SDMA_FLAG_SLEEPING) {
                SDMARestart(); 
            }

            /* Restore the WSF timer from sleep */
            WsfTimerUpdate((WUT_GetSleepTicks() * 1000) / SYS_WUT_GetFreq() / WSF_MS_PER_TICK );

            /* setup the systick */
            SysTick->LOAD = (SystemCoreClock / 1000) * WSF_MS_PER_TICK;
            SysTick->VAL = 0;
            SysTick->CTRL |= (SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_TICKINT_Msk);

        } 
    }

#ifdef DEEPSLEEP_LED 
    LED_Off(DEEPSLEEP_LED);
#endif /* DEEPSLEEP_LED */

    WsfTaskUnlock();
}

/*************************************************************************************************/
/*!
 *  \fn     main
 *
 *  \brief  Entry point for demo software.
 *
 *  \param  None.
 *
 *  \return None.
 */
/*************************************************************************************************/
int main(void)
{
#ifdef ENABLE_SDMA
#ifdef ENABLE_SDMA_ERROR_REPORTING
    uint8_t sdmaFlag;
#endif /* ENABLE_SDMA_ERROR_REPORTING */
#endif /* ENABLE_SDMA */
  
    printf("\n\n***** MAX32665 BLE Fitness Profile, Deep Sleep *****\n");
    printf("SystemCoreClock = %d\n", SystemCoreClock);  
    printf(" A 10sec delay before Adv. (..to allow re-flash)\n");
  
    TMR_Delay(OST_TIMER, INTERVAL_TIME_OST_STARTUP, 0);  
  
    /* Restoring VREGOD when reset out of deep sleep */
    MXC_PWRSEQ->lpvddpd &= ~MXC_F_PWRSEQ_LPVDDPD_VREGODPD;

    /* Run from the 96MHz oscillator */
    switchTo96M();
    Console_Init();

    /* Make sure VRego C uses run voltage. */
    SIMO_setVregO_C(RUN_VOLTAGE);
    while(!(MXC_SIMO->buck_out_ready & MXC_F_SIMO_BUCK_OUT_READY_BUCKOUTRDYC)) {}

#if (BACKUP_MODE == 1)
    printf("Entering backup mode instead of deep sleep\n");
#endif

    /* Initialize Wakeup timer */
    WUT_Init(WUT_PRES_1);
    wut_cfg_t wut_cfg;
    wut_cfg.mode = WUT_MODE_COMPARE;
    wut_cfg.cmp_cnt = 0xFFFFFFFF;

    WUT_Config(&wut_cfg);
    WUT_Enable();

    /* Enable WUT and GPIO as a wakup source */
    MXC_GCR->pm |= (MXC_F_GCR_PM_WUTWKEN | MXC_F_GCR_PM_GPIOWKEN);
    NVIC_EnableIRQ(WUT_IRQn);

    /* Delay before continuing with deep sleep code */
    WUT_Delay_MS(3000);

    /* power off unused hardware */
    DisableUnused();

    /* Initialize Radio */
    WsfInit();
    StackInitFit();
    FitStart();

    /* Setup pushbuttons and timers */
    PB_RegisterRiseFallCallback(0, Button0Pressed);
    PB_RegisterRiseFallCallback(1, Button1Pressed);
    PB_IntEnable(0);

    TMR_Init(BUTTON0_TMR, TMR_PRES_16, NULL);
    TMR_Init(BUTTON1_TMR, TMR_PRES_16, NULL);

    tmr_cfg_t button_config;
    button_config.mode = TMR_MODE_CONTINUOUS;
    TMR_Config(BUTTON0_TMR, &button_config);
    TMR_Config(BUTTON1_TMR, &button_config);

    /* Setup user event (may override PB pin). */
    RegisterUserEvent();
    PalRtcRegisterHook(WUT_IRQHook, NULL);

    /* Register a handler for Application events */
    AppUiActionRegister(SetAddress);

    /* Mark battery level read as not pending. */
    eBasAsyncReadState = BAS_READ_STATE_NONE;

    printf("Setup Complete\n");

#ifdef ENABLE_SDMA
#ifdef ENABLE_SDMA_ERROR_REPORTING
    sdmaFlag = SDMA_FLAG_INIT;
#endif /* ENABLE_SDMA_ERROR_REPORTING */
#endif /* ENABLE_SDMA */
    while (1) {

        wsfOsDispatcher();

#ifdef ENABLE_SDMA
#ifdef ENABLE_SDMA_ERROR_REPORTING
        if (sdmaFlag == SDMA_FLAG_INIT) {
            sdmaFlag = SDMAGetSDMAFlag();
        }
        if (sdmaFlag == SDMA_FLAG_ENOMEM) {
            printf ("SDMA core ran out of allocatable memory.");
            while(1) { }
        }
#endif /* ENABLE_SDMA_ERROR_REPORTING */
#endif /* ENABLE_SDMA */

#ifdef MAIN_LOOP_HANDLER
        /* Handle non-BLE low priority events. */
        HandleUserEvent();
#endif /* MAIN_LOOP_HANDLER */

#ifndef USE_BAS_ASYNC_READ_TIMER_DELAY
        if ((eBasAsyncReadState == BAS_READ_STATE_PENDING) &&
                (wsfOsReadyToSleep()))
        {
            BasReadAsyncHandler();
        }
#endif /* USE_BAS_ASYNC_READ_TIMER_DELAY */

        if (eBasAsyncReadState != BAS_READ_STATE_PENDING)
        {
            #ifdef ENABLE_SDMA
            SDMASleep();
            #else
            Sleep();
            #endif
        }
    }
}

/*****************************************************************/
void HardFault_Handler(void)
{
  __asm(
    " TST LR, #4\n"
        " ITE EQ \n"
        " MRSEQ R0, MSP \n"
        " MRSNE R0, PSP \n"
        " B HardFault_Decoder \n");
}


/*****************************************************************/
void HardFault_Decoder(unsigned int *f)
{

  printf("\n-- HardFault --\n");
  printf("HFSR 0x%08x CFSR 0x%08X", (unsigned int)SCB->HFSR, (unsigned int)SCB->CFSR);
  if (SCB->CFSR & 0x80) {
    printf(", MMFAR 0x%08X", (unsigned int)SCB->MMFAR);
  }
  if (SCB->CFSR & 0x8000) {
    printf(", BFAR 0x%08X", (unsigned int)SCB->BFAR);
  }
  printf("\n");
  
  /* Hang here */
  while(1);
}
