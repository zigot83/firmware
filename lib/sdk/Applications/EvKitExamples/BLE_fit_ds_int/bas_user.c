/*******************************************************************************
 * Copyright (C) 2018 Maxim Integrated Products, Inc., All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL MAXIM INTEGRATED BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of Maxim Integrated
 * Products, Inc. shall not be used except as stated in the Maxim Integrated
 * Products, Inc. Branding Policy.
 *
 * The mere transfer of this software does not imply any licenses
 * of trade secrets, proprietary technology, copyrights, patents,
 * trademarks, maskwork rights, or any other form of intellectual
 * property whatsoever. Maxim Integrated Products, Inc. retains all
 * ownership rights.
 *
 * $Date: 2020-09-08 16:54:25 +0000 (Tue, 08 Sep 2020) $
 * $Revision: 55595 $
 *
 ******************************************************************************/

#define BAS_USER_C

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "mxc_config.h"
#include "mxc_assert.h"
#include "nvic_table.h"
#include "wsf_types.h"
#include "wsf_os.h"
#include "wsf_buf.h"
#include "wsf_timer.h"
#include "wsf_trace.h"
#include "app_ui.h"
#include "app_ui.h"
#include "hci_vs.h"
#include "hci_core.h"
#include "hci_drv_sdma.h"
#include "ipc_defs.h"
#include "pb.h"
#include "tmr.h"
#include "wakeup.h"
#include "uart.h"
#include "sch_api.h"
#include "wut.h"
#include "simo.h"
#include "board.h"
#include "gcr_regs.h"
#include "sch_api_ble.h"
#include "lp.h"
#include "bb_drv.h"
#include "tmr_utils.h"
#include "dm_api.h"
#include "att_api.h"
#include "bas_user.h"

/**************************************************************************************************
  Local Variables
**************************************************************************************************/

/* current battery level read state. */
BAS_READ_STATE eBasAsyncReadState;

/* battery level */
static uint8_t appHwBattLevel;

/**************************************************************************************************
  Functions
**************************************************************************************************/
/**
 * @brief Triggers reporting of the battery level GATT characteristic. */
void BasReadAsyncHandler()
{
#ifdef USE_BAS_ASYNC_READ_TIMER_DELAY
    // Clear interrupt
    TMR_IntClear(OST_TIMER);
#endif /* USE_BAS_ASYNC_READ_TIMER_DELAY */

    /* pretend that we have read 99% */
    appHwBattLevel = 99;

#ifdef USE_BAS_ASYNC_READ_TIMER_DELAY
    // No more interrupts.
    TMR_Disable(OST_TIMER);
    NVIC_DisableIRQ(MXC_TMR_GET_IRQ(OST_TIMER_IDX));
#endif /* USE_BAS_ASYNC_READ_TIMER_DELAY */

    // Request response for battery level GATT characteristic read.
    // Note: This must be set after interrupt is disabled to prevent
    // race condition with BasReadAsyncCback that would result in
    // "missing" responses.
    MXC_ASSERT(eBasAsyncReadState == BAS_READ_STATE_PENDING);
    eBasAsyncReadState = BAS_READ_STATE_READY;
}

// AppHwBattRead (bas_main.c) is split into 2 functions,
// BasRequestAsyncRead to request the read.
// BasReportAsyncRead to report out the read value.

/**
 * @brief  Read the previously read battery level.
 * @return None.
 */
static void BasRequestAsyncRead()
{
#ifdef USE_BAS_ASYNC_READ_TIMER_DELAY
    uint32_t period_ticks = (PeripheralClock*INTERVAL_TIME_OST_BASREAD/4000000UL);
    tmr_cfg_t tmr; 

    /* Make sure timer isn't running */
    TMR_Disable(OST_TIMER);

    /* Configure timer */
    TMR_Init(OST_TIMER, TMR_PRES_4, 0);
    tmr.mode = TMR_MODE_ONESHOT;
    tmr.cmp_cnt = period_ticks;
    tmr.pol = 0;
    TMR_Config(OST_TIMER, &tmr);

    /* Enable timer and make sure it can triggter interrupts. */
    // Note: Enabling interrupts here is contingent on there being
    // no pending response. This is taken care of via
    // switch (bNeedBasAsyncResponse) conditional above.
    eBasAsyncReadState = BAS_READ_STATE_PENDING;
    NVIC_SetVector(MXC_TMR_GET_IRQ(OST_TIMER_IDX), BasReadAsyncHandler);
    NVIC_EnableIRQ(MXC_TMR_GET_IRQ(OST_TIMER_IDX));
    TMR_Enable(OST_TIMER);
#endif /* USE_BAS_ASYNC_READ_TIMER_DELAY */

    eBasAsyncReadState = BAS_READ_STATE_PENDING;
}

/**
 * @brief  Report the previously read battery level.
 *
 *          Value returned in pLevel is the percentage of
 *          remaining battery capacity (0-100%).
 *          
 * @param  pLevel Battery level return value.
 * @return None.
 */
static void BasReportAsyncRead(uint8_t *pLevel)
{
    *pLevel = appHwBattLevel;
}

/**
 * @brief  Report the previously read battery level.
 *
 *          Value returned in pLevel is a known bad value for
 *          remaining battery capacity (101%).
 *          
 * @param  pLevel Battery level return value.
 * @return None.
 */
static void BasReportInvalidRead(uint8_t *pLevel)
{
    *pLevel = (uint8_t)(int8_t)-1;
}

/**
 * @brief Application specific battery level GATT characteristic read callback.
 * @return Status indicating that the response is pending. */
uint8_t BasReadAsyncCback(dmConnId_t connId, uint16_t handle, uint8_t operation,
                     uint16_t offset, attsAttr_t *pAttr)
{
    uint8_t response;

    switch (eBasAsyncReadState)
    {
        case BAS_READ_STATE_NONE:
            /* Issue request to read battery level and
             * trigger battery read response state transition
             * to BAS_READ_STATE_PENDING. */
            BasRequestAsyncRead();

             /* Wait for the application to report the battery level. */
            BasReportInvalidRead(pAttr->pValue);
            response = BAS_ASYNC_READ_PENDING_RSP;
            break;

        case BAS_READ_STATE_PENDING:
            /* Only one response per request. */
            BasReportInvalidRead(pAttr->pValue);
            response = BAS_ASYNC_READ_PENDING_RSP;
            break;

        case BAS_READ_STATE_READY:
            /* Report the previously read battery level attribute value. */
            // Note: It is assumed that the BasReportAsyncRead does not have
            // a significant delay, and simply reads the value that has
            // already been read in response to BasRequestAsyncRead.
            BasReportAsyncRead(pAttr->pValue);

            /* Wait for the application to report the battery level. */
            response = ATT_SUCCESS;

            /* Return to ready state */
            eBasAsyncReadState = BAS_READ_STATE_NONE;

            break;

        default:
            // Invalid state.
            MXC_ASSERT(((void)eBasAsyncReadState, 0));
            response = ATT_ERR_UNDEFINED;
    }

    return response;
}

