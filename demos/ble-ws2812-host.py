#!/usr/bin/env python3
import bluepy
import time
import colorsys


# Change this to the MAC of your card10
p = bluepy.btle.Peripheral("CA:4D:10:01:ff:64")
c = p.getCharacteristics(
    uuid='23238001-2342-2342-2342-234223422342')[0]

hue = 0
while 1:
    r,g,b = colorsys.hsv_to_rgb(hue, 1, 0.1)
    c.write(b"%c%c%c" %
        (int(r*255), int(g*255), int(b*255)), True)
    time.sleep(.1)
    hue += 0.1

