import ws2812, gpio, bluetooth, time, display
from micropython import const

_IRQ_GATTS_WRITE = const(3)

WS2812_SERVICE_UUID = \
    bluetooth.UUID("23238000-2342-2342-2342-234223422342")
SET_ALL = (
    bluetooth.UUID("23238001-2342-2342-2342-234223422342"),
    bluetooth.FLAG_WRITE
)
WS2812_SERVICE = (
    WS2812_SERVICE_UUID,
    (SET_ALL,)
)

def irq(event, data):
    if event == _IRQ_GATTS_WRITE:
        conn_handle, value_handle = data
        value = ble.gatts_read(value_handle)
        ws2812.set_all(gpio.WRISTBAND_3, [value] * 3)

if __name__ == "__main__":
    display.open().backlight(0)
    gpio.set_mode(gpio.WRISTBAND_3, gpio.mode.OUTPUT)

    ble = bluetooth.BLE()
    ble.active(True)
    ble.irq(irq)
    ble.gatts_register_services((WS2812_SERVICE,))

    print("Waiting for connection!")

    while True:
        time.sleep(1)
