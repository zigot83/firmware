#!/bin/sh
set -e

PYTHON="$1"
BIN1="$2"
BIN2="$3"
BINOUT="$4"

if [ "$(stat -c "%s" "${BIN1}")" -gt 589824 ]; then
    echo "$0: ${BIN1} is too big to fit!" >&2
    exit 1
fi

objcopy -I binary -O binary --pad-to=589824 --gap-fill=255 "${BIN1}" "$BINOUT"
cat "$BIN2" >>"$BINOUT"

"$PYTHON" "$(dirname "$0")/crc_patch.py" "$BINOUT"
