import sys_png
import color

RGB8 = 0
RGBA8 = 1
RGB565 = 2
RGBA5551 = 3


def decode(png_data, format="RGB", bg=color.BLACK):
    """
    Decode a PNG image and return raw pixel data.

    :param str format: The intended output format:

       - ``png.RGB8``: 24 bit RGB.
       - ``png.RGBA8``: 24 bit RGB + 8 bit alpha.
       - ``png.RGB565``: 16 bit RGB. This consumes 1 byte less RAM per pixel than ``png.RGB8``.
       - ``png.RGBA5551``: 15 bit RGB + 1 bit alpha.

       Default is ``png.RGB8``.

    :param Color bg: Background color.

      If the PNG contains an alpha channel but no alpha
      channel is requested in the output (``png.RGB8`` or ``png.RGB565``)
      this color will be used as the background color.

      Default is ``Color.BLACK``.

    :returns: Typle ``(width, height, data)``

    .. versionadded:: 1.17

    **Example with RGBA8 data:**

    .. code-block:: python

       import display
       import png

       # Draw a PNG file to the display
       f = open("example.png")
       w, h, img = png.decode(f.read(), png.RGBA8)
       f.close()
       with display.open() as d:
           d.clear()
           d.blit(0, 0, w, h, img, display.RGBA8)
           d.update()


    **Example with RGB565 data:**

    .. code-block:: python

       import display
       import png

       # Draw a PNG file to the display
       f = open("example.png")
       w, h, img = png.decode(f.read(), png.RGB565)
       f.close()
       with display.open() as d:
           d.clear()
           d.blit(0, 0, w, h, img, True, display.RGB565)
           d.update()

    """

    formats = (RGB8, RGBA8, RGB565, RGBA5551)
    if format not in formats:
        raise ValueError("Output format not supported")

    if format == RGB8:
        return sys_png.decode(png_data, 1, 0, bg)
    if format == RGBA8:
        return sys_png.decode(png_data, 1, 1, bg)
    if format == RGB565:
        return sys_png.decode(png_data, 0, 0, bg)
    if format == RGBA5551:
        return sys_png.decode(png_data, 0, 1, bg)
