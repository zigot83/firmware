#include "epicardium.h"

#define LODEPNG_NO_COMPILE_ENCODER
#define LODEPNG_NO_COMPILE_DISK
#define LODEPNG_NO_COMPILE_ALLOCATORS
#include "lodepng.h"

#include "py/builtin.h"
#include "py/binary.h"
#include "py/obj.h"
#include "py/objarray.h"
#include "py/runtime.h"
#include "py/gc.h"

void *lodepng_malloc(size_t size)
{
	return m_malloc(size);
}

void *lodepng_realloc(void *ptr, size_t new_size)
{
	return m_realloc(ptr, new_size);
}

void lodepng_free(void *ptr)
{
	m_free(ptr);
}

static void lode_raise(unsigned int status)
{
	if (status) {
		nlr_raise(mp_obj_new_exception_msg_varg(
			&mp_type_ValueError, lodepng_error_text(status))
		);
	}
}

static inline uint16_t rgba8_to_rgba5551(uint8_t *bytes)
{
	uint16_t c = ((bytes[0] & 0b11111000) << 8) |
		     ((bytes[1] & 0b11111000) << 3) |
		     ((bytes[2] & 0b11111000) >> 2);
	if (bytes[3] > 127) {
		c |= 1;
	}
	return c;
}

static inline uint16_t rgb8_to_rgb565(uint8_t *bytes)
{
	return ((bytes[0] & 0b11111000) << 8) | ((bytes[1] & 0b11111100) << 3) |
	       (bytes[2] >> 3);
}

static inline uint8_t apply_alpha(uint8_t in, uint8_t bg, uint8_t alpha)
{
	/* Not sure if it is worth (or even a good idea) to have
	 * the special cases here. */
	if (bg == 0) {
		return (in * alpha) / 255;
	}

	uint8_t beta = 255 - alpha;

	if (bg == 255) {
		return ((in * alpha) / 255) + beta;
	}

	return (in * alpha + bg * beta) / 255;
}

static mp_obj_t mp_png_decode(size_t n_args, const mp_obj_t *args)
{
	mp_buffer_info_t png_info;

	mp_obj_t png       = args[0];
	mp_obj_t rgb8_out  = args[1];
	mp_obj_t alpha_out = args[2];
	mp_obj_t bg        = args[3];

	/* Load buffer and ensure it contains enough data */
	if (!mp_get_buffer(png, &png_info, MP_BUFFER_READ)) {
		mp_raise_TypeError("png does not support buffer protocol.");
	}

	if (mp_obj_get_int(mp_obj_len(bg)) < 3) {
		mp_raise_ValueError("bg must have 3 elements.");
	}

	int i, j;

	unsigned int w, h;
	uint8_t *raw;
	int raw_len;
	int raw_len_original;

	LodePNGState state;
	lodepng_state_init(&state);
	state.decoder.ignore_crc = 1;
	lode_raise(lodepng_inspect(&w, &h, &state, png_info.buf, png_info.len));

	unsigned alpha_in = lodepng_can_have_alpha(&(state.info_png.color));

	/* Do we need to consider an alpha channel? */
	if (alpha_in || mp_obj_is_true(alpha_out)) {
		lode_raise(lodepng_decode32(
			&raw, &w, &h, png_info.buf, png_info.len)
		);
		raw_len = w * h * 4;
	} else {
		lode_raise(lodepng_decode24(
			&raw, &w, &h, png_info.buf, png_info.len)
		);
		raw_len = w * h * 4;
	}

	raw_len_original = raw_len;

	/* User did not ask for alpha, but input might contain alpha.
	 * Remove alpha using provided background color. */
	if (alpha_in && !mp_obj_is_true(alpha_out)) {
		uint8_t bg_red = mp_obj_get_int(
			mp_obj_subscr(bg, mp_obj_new_int(0), MP_OBJ_SENTINEL)
		);
		uint8_t bg_green = mp_obj_get_int(
			mp_obj_subscr(bg, mp_obj_new_int(1), MP_OBJ_SENTINEL)
		);
		uint8_t bg_blue = mp_obj_get_int(
			mp_obj_subscr(bg, mp_obj_new_int(2), MP_OBJ_SENTINEL)
		);

		for (i = 0, j = 0; i < raw_len; i += 4, j += 3) {
			uint8_t alpha = raw[i + 3];
			raw[j]        = apply_alpha(raw[i], bg_red, alpha);
			raw[j + 1] = apply_alpha(raw[i + 1], bg_green, alpha);
			raw[j + 2] = apply_alpha(raw[i + 2], bg_blue, alpha);
		}
		raw_len = w * h * 3;
	}

	if (!mp_obj_is_true(rgb8_out)) {
		if (mp_obj_is_true(alpha_out)) {
			for (i = 0, j = 0; i < raw_len; i += 4, j += 2) {
				uint16_t c = rgba8_to_rgba5551(&raw[i]);
				raw[j]     = c & 0xFF;
				raw[j + 1] = c >> 8;
				raw[j + 2] = raw[i + 3];
			}
		} else {
			for (i = 0, j = 0; i < raw_len; i += 3, j += 2) {
				uint16_t c = rgb8_to_rgb565(&raw[i]);
				raw[j]     = c & 0xFF;
				raw[j + 1] = c >> 8;
			}
		}
		raw_len = w * h * 2;
	}

	if (raw_len != raw_len_original) {
		/* Some conversion shrank the buffer.
		 * Reallocate to free the unneeded RAM. */
		m_realloc(raw, raw_len);
	}

	mp_obj_t mp_w   = MP_OBJ_NEW_SMALL_INT(w);
	mp_obj_t mp_h   = MP_OBJ_NEW_SMALL_INT(h);
	mp_obj_t mp_raw = mp_obj_new_memoryview(
		MP_OBJ_ARRAY_TYPECODE_FLAG_RW | BYTEARRAY_TYPECODE,
		raw_len,
		raw
	);
	mp_obj_t tup[] = { mp_w, mp_h, mp_raw };

	return mp_obj_new_tuple(3, tup);
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(decode, 4, 4, mp_png_decode);

static const mp_rom_map_elem_t png_module_globals_table[] = {
	{ MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_sys_png) },
	{ MP_ROM_QSTR(MP_QSTR_decode), MP_ROM_PTR(&decode) },
};
static MP_DEFINE_CONST_DICT(png_module_globals, png_module_globals_table);

// Define module object.
const mp_obj_module_t png_module = {
	.base    = { &mp_type_module },
	.globals = (mp_obj_dict_t *)&png_module_globals,
};

/* Register the module to make it available in Python */
/* clang-format off */
MP_REGISTER_MODULE(MP_QSTR_sys_png, png_module, MODULE_PNG_ENABLED);
