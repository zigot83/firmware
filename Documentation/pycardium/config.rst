``config`` - Configuration
==========================
The ``config`` module provides functions to interact with card10's
configuration file (:ref:`card10_cfg`).

.. automodule:: config
   :members:


