.. _bluetooth_ecg_service:

Bluetooth ECG Service
========================

.. warning::
    The service is still work in progress and subject to change

The ECG service provides access to the ECG sensor of the card10

BLE Service
-----------

The current draft uses following service specification:

- Service:

  UUID: ``42230300-2342-2342-2342-234223422342``

- ECG samples characteristic:

  UUID: ``42230301-2342-2342-2342-234223422342``
  notify

ECG samples characteristic
--------------------------

List of 16 bit samples (big endian). Enable notifications to
receive a stream of samples while the ECG app is open.

The first 16 bit are a sample counter (big endian).
