.. _bluetooth_overview:

Overview
========

UUIDs
-----
Bluetooth uses UUIDs to identify almost everything. The Bluetooth SIG specifies
a number of "short" 16-bit UUIDs for general use. A device is free to define
other "long" 128-bit UUIDs if it wants to express non-standard functionality.

The card10 defines a few non-standard UUIDs. They are prefixed with ``4232``:


+--------------------------------------+-------------------------------------------------------+--------------------------------------------------------------------+
| UUID                                 | Usage                                                 | Comment                                                            |
+======================================+=======================================================+====================================================================+
| 422301XX-2342-2342-2342-234223422342 | :ref:`File Transfer Service<bluetooth_file_transfer>` | Used by the Companion App to install apps on the card10            |
+--------------------------------------+-------------------------------------------------------+--------------------------------------------------------------------+
| 422302XX-2342-2342-2342-234223422342 | :ref:`card10 Service<bluetooth_card10_service>`       | Controls general card10 functionality like LEDs and personal state |
+--------------------------------------+-------------------------------------------------------+--------------------------------------------------------------------+
| 422303XX-2342-2342-2342-234223422342 | :ref:`ECG Service<bluetooth_ecg_service>`             | Allows to stream ECG measurements via BLE                          |
+--------------------------------------+-------------------------------------------------------+--------------------------------------------------------------------+
| 422380XX-2342-2342-2342-234223422342 | Experimental usage                                    | To be used for development and experiments                         |
+--------------------------------------+-------------------------------------------------------+--------------------------------------------------------------------+

The first byte after the prefix identifies the service (e.g. ``03`` for ECG).
The next byte is reserved for UUIDs used by the service itself. The service
uses ``00`` at this position. The first attribute inside the service uses
``01`` at this position. See the documentation of any service for an example.

You can use the UUID range 422380XX-2342-2342-2342-234223422342 for your own
experiments / demos/ development. For example the demos under `demos/` in the
repository use this range of UUIDs.

BLE with MicroPython
--------------------

MicroPython docs: https://docs.micropython.org/en/latest/library/bluetooth.html

Basic example: https://git.card10.badge.events.ccc.de/card10/firmware/-/blob/master/demos/ble-ws2812-card10.py and https://git.card10.badge.events.ccc.de/card10/firmware/-/blob/master/demos/ble-ws2812-host.py

MicroPython examples: https://github.com/micropython/micropython/tree/7c54b6428058a236b8a48c93c255948ece7e718b/examples/bluetooth
