#pragma once

#include <stdbool.h>
#include <stdint.h>

void lcd_set_sleep(bool sleep);
void lcd_reconfigure(void);
void lcd_initialize(void);
void lcd_write_fb_partial(
	uint16_t xstart,
	uint16_t ystart,
	uint16_t xend,
	uint16_t yend,
	const uint8_t *fb
);
void lcd_write_fb(const uint8_t *fb);
void lcd_set_screenflip(bool flipped);
